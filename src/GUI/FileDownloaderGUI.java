/*
 * Created by JFormDesigner on Thu Nov 26 04:56:09 GMT 2015
 */

package GUI;

import downloader.FilesDownloadExecutor;
import parse.WebPageParser;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * @author Saifullah Azmi
 */
public class FileDownloaderGUI extends JPanel {

    private WebPageParser parser;
    private FilesDownloadExecutor download;

    public FileDownloaderGUI() {
        initComponents();
        initFileDownloader();
    }

    private void initFileDownloader() {

        folderSelectionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser saveFolder = new JFileChooser();
                saveFolder.showOpenDialog(null);
                saveFolder.setDialogTitle("Save downloads to...");
                saveFolder.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                saveFolder.setAcceptAllFileFilterUsed(false);
                saveFolderField.setText(saveFolder.getCurrentDirectory().getAbsolutePath() + "/");
            }
        });

        fileTypeSelector.setModel(new DefaultComboBoxModel<>(WebPageParser.fileExtensionsList));

        String[] columns = new String[]{
                "S.No.", "File Name", "Status", "URL"
        };
        DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        fileStatusTable.setModel(tableModel);

        startDownloadsButton.addActionListener(e -> {
            initParse(tableModel);
            initDownload(tableModel)
        });

        exitButton.addActionListener(e -> System.exit(0));
    }

    private void initParse(DefaultTableModel tableModel) {
        tableModel.setRowCount(0);
        Thread parseThread = new Thread() {

            @Override
            public void run() {
                parser = new WebPageParser(webPageField.getText(),
                        fileTypeSelector.getSelectedItem().toString());

                SwingUtilities.invokeLater(() -> {

                    for (Object[] row : parser.getDownloadTableRows()) {
                        tableModel.addRow(row);
                    }
                });
            }
        };
    }
    private void initDownload(DefaultTableModel tableModel) {

        Thread downloadThread = new Thread() {

            @Override
            public void run() {
                download = new FilesDownloadExecutor(Integer.parseInt(threadPoolArea.getText()),
                        parser.getFileUrlList(),
                        saveFolderField.getText());

            }
        };

        downloadThread.start();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Saifullah Azmi
        userFormPanel = new JPanel();
        webPageLabel = new JLabel();
        saveFolderLabel = new JLabel();
        webPageField = new JTextField();
        saveFolderField = new JTextField();
        folderSelectionButton = new JButton();
        fileTypeLabel = new JLabel();
        fileTypeSelector = new JComboBox();
        label3 = new JLabel();
        threadPoolArea = new JTextField();
        controllPanel = new JPanel();
        startDownloadsButton = new JButton();
        exitButton = new JButton();
        scrollPane1 = new JScrollPane();
        fileStatusTable = new JTable();

        //======== this ========

        // JFormDesigner evaluation mark
        setBorder(new javax.swing.border.CompoundBorder(
            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

        setLayout(new BorderLayout());

        //======== userFormPanel ========
        {
            userFormPanel.setLayout(null);

            //---- webPageLabel ----
            webPageLabel.setText("Web Page:");
            userFormPanel.add(webPageLabel);
            webPageLabel.setBounds(35, 15, 85, 30);

            //---- saveFolderLabel ----
            saveFolderLabel.setText("Save Folder:");
            userFormPanel.add(saveFolderLabel);
            saveFolderLabel.setBounds(35, 55, 89, 30);
            userFormPanel.add(webPageField);
            webPageField.setBounds(125, 15, 805, 30);
            userFormPanel.add(saveFolderField);
            saveFolderField.setBounds(125, 55, 750, 30);

            //---- folderSelectionButton ----
            folderSelectionButton.setText("...");
            userFormPanel.add(folderSelectionButton);
            folderSelectionButton.setBounds(880, 55, folderSelectionButton.getPreferredSize().width, 30);

            //---- fileTypeLabel ----
            fileTypeLabel.setText("File Type:");
            userFormPanel.add(fileTypeLabel);
            fileTypeLabel.setBounds(35, 100, fileTypeLabel.getPreferredSize().width, 33);
            userFormPanel.add(fileTypeSelector);
            fileTypeSelector.setBounds(125, 100, 90, 33);

            //---- label3 ----
            label3.setText("Paralell Downloads: ");
            userFormPanel.add(label3);
            label3.setBounds(300, 100, label3.getPreferredSize().width, 30);
            userFormPanel.add(threadPoolArea);
            threadPoolArea.setBounds(445, 100, 65, 31);

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < userFormPanel.getComponentCount(); i++) {
                    Rectangle bounds = userFormPanel.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = userFormPanel.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                userFormPanel.setMinimumSize(preferredSize);
                userFormPanel.setPreferredSize(preferredSize);
            }
        }
        add(userFormPanel, BorderLayout.NORTH);

        //======== controllPanel ========
        {
            controllPanel.setLayout(null);

            //---- startDownloadsButton ----
            startDownloadsButton.setText("Download");
            controllPanel.add(startDownloadsButton);
            startDownloadsButton.setBounds(680, 10, 130, 34);

            //---- exitButton ----
            exitButton.setText("Exit");
            controllPanel.add(exitButton);
            exitButton.setBounds(830, 10, 100, 34);

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < controllPanel.getComponentCount(); i++) {
                    Rectangle bounds = controllPanel.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = controllPanel.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                controllPanel.setMinimumSize(preferredSize);
                controllPanel.setPreferredSize(preferredSize);
            }
        }
        add(controllPanel, BorderLayout.SOUTH);

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(fileStatusTable);
        }
        add(scrollPane1, BorderLayout.CENTER);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Saifullah Azmi
    private JPanel userFormPanel;
    private JLabel webPageLabel;
    private JLabel saveFolderLabel;
    private JTextField webPageField;
    private JTextField saveFolderField;
    private JButton folderSelectionButton;
    private JLabel fileTypeLabel;
    private JComboBox fileTypeSelector;
    private JLabel label3;
    private JTextField threadPoolArea;
    private JPanel controllPanel;
    private JButton startDownloadsButton;
    private JButton exitButton;
    private JScrollPane scrollPane1;
    private JTable fileStatusTable;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
