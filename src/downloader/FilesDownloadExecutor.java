package downloader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaThreading
 * @date : 26/11/15
 */

/**
 * An ExecutorService framework to manage the thread pool and job queue.
 */
public class FilesDownloadExecutor {

    private static final Logger log = Logger.getLogger(FilesDownloadExecutor.class.getName());

    private int threadPoolSize;
    private List<String> fileURLs;
    private String saveFolder;

    private List<Future<Integer>> threadFuturesList;

    /**
     * Initialises the executor with the parsed data.
     * @param threadPoolSize the number of parallel downloads
     * @param fileURLs list of file urls to be downloaded
     * @param saveFolder folder location to save the downloaded files.
     */
    public FilesDownloadExecutor(int threadPoolSize, List<String> fileURLs, String saveFolder) {
        this.threadPoolSize = threadPoolSize;
        this.fileURLs = fileURLs;
        this.saveFolder = saveFolder;

        this.threadFuturesList = new ArrayList<>();
        beginDownload();
    }

    /**
     * Starts the download process
     */
    private void beginDownload() {

        // Initialises the thread pool with a set number of threads.
        ExecutorService downloadPool = Executors.newFixedThreadPool(this.threadPoolSize);

        // For every URL
        for (String fileURL : fileURLs) {

            // Create a new task
            FileDownloaderCallable fileDownloadCallable = new FileDownloaderCallable(fileURL, saveFolder);
            // Assign it to the job queue to wait for available thread
            Future<Integer> future = downloadPool.submit(fileDownloadCallable);
            // Storing objects for later reference.
            threadFuturesList.add(future);
        }

        // Closes the thread pool.
        downloadPool.shutdown();
        try {
            downloadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            log.log(Level.SEVERE, e.toString(), e);
        }
    }

    /**
     * Gets the list of future objects of threads.
     * @return a list of future objects
     */
    public List<Future<Integer>> getThreadFuturesList() {
        return this.threadFuturesList;
    }
}
