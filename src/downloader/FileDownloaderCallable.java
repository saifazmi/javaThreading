package downloader;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaThreading
 * @date : 26/11/15
 */

/**
 * Represents a callable object which can be a potential thread
 */
public class FileDownloaderCallable implements Callable {

    private static final Logger log = Logger.getLogger(FileDownloaderCallable.class.getName());

    private String fileURL;
    private String saveFolder;
    private String fileName;

    /**
     * Constructs the callable object with right parameters for the task
     * @param fileURL the file to be downloaded
     * @param saveFolder the folder location for saving the file
     */
    public FileDownloaderCallable(String fileURL, String saveFolder) {
        this.fileURL = fileURL;
        this.saveFolder = saveFolder;
        this.fileName = fileURL.substring(fileURL.lastIndexOf('/') + 1, fileURL.length());
    }

    /**
     * Downloads the file from a URL.
     * @return a thread flag.
     */
    @Override
    public Integer call() {

        // this flag represents the state of the thread
        // 1 = in Queue
        // 0 = Complete
        // -1 = Error
        Integer threadFlag = 1;
        Thread.currentThread().setName(fileName);
        try {
            // Opening a URL stream
            URL url = new URL(fileURL);
            try (InputStream in = url.openStream()) {
                try (OutputStream out = new BufferedOutputStream(new FileOutputStream(saveFolder + fileName))) {
                    for (int i; (i = in.read()) != -1; ) {
                        // writing file to local drive.
                        out.write(i);
                        threadFlag = 0;
                    }
                }
            } catch (IOException e) {
                threadFlag = -1;
                log.log(Level.SEVERE, e.toString(), e);
            }
        } catch (MalformedURLException e) {
            threadFlag = -1;
            log.log(Level.SEVERE, e.toString(), e);
        }

        return threadFlag;
    }

}
