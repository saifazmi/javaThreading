package parse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaThreading
 * @date : 25/11/15
 */

/**
 * Handles all the parsing related tasks for the web pages submitted.
 */
public class WebPageParser {

    private static final Logger log = Logger.getLogger(WebPageParser.class.getName());

    // An array to act as a model for the combo box.
    public static final String[] fileExtensionsList = new String[]{
            "jpg","gif","png","bmp",    // images
            "mp4","ogg","flv","avi",    // videos
            "mp3","wav","wma",          // audio
            "pdf","txt","doc","xls",    // documents
            "zip","rar","7zip"          // compressed
    };

    private String webPage;
    private String fileType;

    private List<String> fileUrlList;
    private List<Object[]> downloadTableRows;

    /**
     * Constructs the parser with proper filters and resources.s
     * @param webPage the web page to be parsed.
     * @param fileType the file types to be fetched.
     */
    public WebPageParser(String webPage, String fileType) {
        this.webPage = webPage;
        this.fileType = fileType;
        this.fileUrlList = new ArrayList<>();
        this.downloadTableRows = new ArrayList<>();
        parse();
    }

    /**
     * Performs the parse and generates a list of URLs.
     */
    private void parse() {
        Document doc;

        try {
            doc = Jsoup.connect(webPage).get();

            Elements links = doc.select("a[href~=(?i)\\.(" + fileType + ")]");

            int fileCount = 0;

            for (Element link : links) {
                String urlstr = link.attr("href");
                // to deal with external links
                if (urlstr.contains("http://")
                        || urlstr.contains("https://")) {
                    this.fileUrlList.add(urlstr);
                }
                // to deal with inert links
                else if (urlstr.indexOf(webPage) <= 0) {
                    urlstr = webPage + urlstr;
                    this.fileUrlList.add(urlstr);
                }

                Object[] downloadRow = {
                        ++fileCount,
                        urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length()),
                        "",
                        ""
                };

                downloadTableRows.add(downloadRow);
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, e.toString(), e);

        }
    }

    /**
     * Gets the list of URLs to be downloaded
     * @return a list of URLs.
     */
    public List<String> getFileUrlList() {
        return this.fileUrlList;
    }

    /**
     * Gets the list of rows for the GUI table.
     * @return  a list of rows for Jtable.
     */
    public List<Object[]> getDownloadTableRows() {
        return downloadTableRows;
    }
}
